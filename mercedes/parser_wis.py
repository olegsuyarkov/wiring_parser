from cv2 import WINDOW_FULLSCREEN
from numpy import true_divide
from pyscreeze import locateOnScreen
import tabula as tb
import pandas as pd
import pyautogui as pi
import keyboard as kb
import os
import shutil
import re
import logging
import sys

logging.basicConfig(level=logging.INFO, format='%(levelname)s:%(message)s')


def convert_pdf_to_json(pdf_path, output_file, output_folder):
    data = tb.read_pdf(pdf_path, pages='all', stream=True, area=[11.5, 30.5, 805.5, 562.8], columns=[128.8, 295.8, 461.9], pandas_options={'header': None})
    df = pd.concat(data)
    df.columns = ['1', '2', '3', 'doc']
    df.reset_index(drop=True, inplace=True)
    df = df[df['doc'].notna()]
    regex = '[A-Z]\d\d[.]\d\d[-][A-Z]'
    df = df[df['doc'].str.contains(regex)]
    df = df.drop_duplicates(subset='doc', keep='last')
    df.reset_index(drop=True, inplace=True)
    # print(df.to_string())
    for index, row in df.iterrows():
        if isinstance(row['1'], str):
            if len(row['1']) > len(row['doc']):
                # if re.search('[A-Z]\d\d[.]\d\d[-][A-Z]', row['1']):
                if row['doc'] in row['1']:
                    df.at[index, 'doc'] = row['1']
    
    # print(df.to_string())
    df.to_json(path_or_buf=output_folder+output_file)
    # df.to_csv(path_or_buf=output_folder+output_file, index=False)


def clickbot(jsonfile, docname):
    logging.info(f'>>>>>>>>>> clickbot start work with --> "{docname}" <<<<<<<<<<')
    # write name and press enter
    kb.write(docname)
    pi.sleep(0.3)
    kb.send('enter')
    pi.sleep(1)

    # check incomplite docname
    not_found = 'mercedes/not_found.PNG'
    print_document = 'mercedes/print_document.PNG'
    print_wiring = 'mercedes/print_wiring.PNG'
    subfolder = 'passenger'
    work_dir = 'd:/Wiring Diagrams/Mercedes/{}'.format(subfolder)
    # incomplite_window = 'mercedes/incomplite.PNG'
    # if pi.locateOnScreen(incomplite_window, confidence=0.9):
    #     print('incomplite')
    #     kb.send('enter')

    #     # click to search area
    #     pi.click(363,170)

    #     # select all in area and delete all
    #     kb.send('ctrl+a')
    #     kb.send('delete')
    # # check not found docname
    if pi.locateOnScreen(not_found,  confidence=0.9):
        with open(work_dir+'/not_found.txt', 'a') as file:
            file.write(jsonfile+'::'+docname)
        logging.info(f'"{docname}" --> not found by clickbot')
        # determine button
        pi.click(150,141)

        # click to search area
        pi.click(363,170)

        # select all in area and delete all
        kb.send('ctrl+a')
        kb.send('delete')
    else:
        logging.info(f'"{docname}" --> was found by clickbot')
        # click to print button
        pi.click(1018,70)
        pi.sleep(0.5)

        while True:
            if locateOnScreen(print_document, confidence=0.9) is None:
                pi.sleep(0.5)
                continue
            else:
                break

        # check if document is wiring diagram
        if locateOnScreen(print_wiring, confidence=0.9) is not None:
            fit_to_page = 'mercedes/fit_to_page.PNG'
            x_checkbox_fit, y_checkbox_fix = pi.locateCenterOnScreen(fit_to_page, confidence=0.7)
            pi.click(x_checkbox_fit, y_checkbox_fix)
            with open(work_dir+'/wiring_diagrams.txt', 'a') as file:
                file.write(jsonfile+'::'+docname)
            logging.info(f'"{docname}" --> is wiring diagram, add to wiring list')

        # click print
        pi.click(520,607)
        pi.sleep(0.5)

        # click OK on print window
        pi.click(367,371)

        # wait for final save window / find save button
        btn_screen = 'mercedes/save_btn.PNG'
        pi.sleep(1)
        while True:
            if pi.locateOnScreen(btn_screen, confidence=0.9) is None:
                pi.sleep(0.5)
                continue
            else:
                break
        kb.write(docname)
        # click to save btn
        # pi.click(762,785)
        pi.sleep(0.2)
        save_single_btn = 'mercedes/save_single_btn.PNG'
        x_save_btn, y_save_btn = pi.locateCenterOnScreen(save_single_btn, confidence=0.7)
        pi.click(x_save_btn, y_save_btn)
        pi.sleep(0.5)

        # check exist file
        exist_file = 'mercedes/exist.PNG'
        no_btn = 'mercedes/no_btn.PNG'
        cancel_btn = 'mercedes/cancel_btn.PNG'
        if pi.locateOnScreen(exist_file, confidence=0.9):
            logging.info(f'"{docname}" --> is already exist, clickbot don\'t save file')
            # click to no_btn
            # pi.click(758,502)
            x_no_btn, y_no_btn = pi.locateCenterOnScreen(no_btn, confidence=0.7)
            pi.click(x_no_btn, y_no_btn)
            pi.sleep(0.2)
            # click to cancel_btn
            # pi.click(856,787)
            x_cancel_btn, y_cancel_btn = pi.locateCenterOnScreen(cancel_btn, confidence=0.9)
            pi.click(x_cancel_btn, y_cancel_btn)

        # click to start search
        pi.click(140,139)

        # click to search area
        pi.click(363,170)

        # select all in area and delete all
        kb.send('ctrl+a')
        kb.send('delete')
        logging.info(f'>>>>>>>>>> clickbot finish work with --> "{docname}" <<<<<<<<<<')


def work_loop():
    # get files in pdf folder
    subfolder = 'passenger'
    pdf_dir = 'd:/Wiring Diagrams/Mercedes/{}/pdf'.format(subfolder)
    process_pdf_dir = 'd:/Wiring Diagrams/Mercedes/{}/processed_pdf/'.format(subfolder)
    json_dir = 'd:/Wiring Diagrams/Mercedes/{}/json/'.format(subfolder)
    process_json_dir = 'd:/Wiring Diagrams/Mercedes/{}/processed_json/'.format(subfolder)

    # get start flag
    start_from_json = False
    if len(sys.argv) > 1:
        if sys.argv[1] == 'json':
            start_from_json = True

    counter = 0
    while counter < 100:
        unwork_pdf = os.listdir(pdf_dir)
        unwork_json_files = [s.replace('.pdf', '') for s in unwork_pdf]
        if start_from_json is False:
            if len(unwork_pdf) != 0:
                # process every file
                for unwork_pdf_file in unwork_pdf:
                    logging.info(f'---------- start iterate convert pdf --> "{unwork_pdf_file}" --> to json ----------')
                    convert_pdf_to_json(pdf_dir+'/'+unwork_pdf_file, unwork_pdf_file.replace('.pdf', '.json'), json_dir)
                    # move pdf to process pdf folder
                    try:
                        shutil.move(pdf_dir+'/'+unwork_pdf_file, process_pdf_dir)
                    except shutil.Error:
                        os.remove(pdf_dir+'/'+unwork_pdf_file)
                        # print('file exist')
                    logging.info(f'---------- finish iterate convert --> "{unwork_pdf_file}" ----------')

            else:
                logging.info('empty in pdf folder')
                counter = counter+1
                logging.info(f'counter+1 in pdf, counter={counter}')

        start_from_json = False
        unwork_json = os.listdir(json_dir)
        if len(unwork_json) != 0:
            for unwork_json_file in unwork_json:
                # get list of process json files
                logging.info(f'---------- start iterate json file --> "{unwork_json_file}" ----------')
                worked_json = [i.replace('.json', '') for i in os.listdir(process_json_dir)]
                if unwork_json_file.replace('.json', '') in worked_json:
                    logging.info(f'duplicate json file in work and unwork directory, file name is --> "{unwork_json_file}"')
                    try:
                        shutil.move(json_dir+'/'+unwork_json_file, process_json_dir)
                    except shutil.Error:
                        os.remove(json_dir+'/'+unwork_json_file)
                        # print('file exist')
                else:
                    logging.info(f'read json file --> "{unwork_json_file}"')
                    df = pd.read_json(json_dir+'/'+unwork_json_file)
                    df = df[~df['doc'].isin(worked_json)]
                    df = df[~df['doc'].isin(unwork_json_files)]
                    if len(df.index) != 0:
                        # click to wis
                        pi.click(468,1007)
                        for index, row in df.iterrows():
                            clickbot(unwork_json_file, row['doc'])
                        # click to VS Code
                        pi.click(395,1008)
                    try:
                        shutil.move(json_dir+'/'+unwork_json_file, process_json_dir)
                    except shutil.Error:
                        os.remove(json_dir+'/'+unwork_json_file)
                        # print('file exist')
                logging.info(f'---------- finish iterate json file --> "{unwork_json_file}" ----------')
        else:
            logging.info('empty in json folder')
            counter = counter+1
            logging.info(f'counter+1 in json, counter={counter}')


# convert pdf_to_json
def test_1():
    passenger_entry_file = 'D:/Wiring Diagrams/Mercedes/passenger/pdf/file3.pdf'
    output_file = 'file3.json'
    output_folder = 'D:/Wiring Diagrams/Mercedes/passenger/json/'
    convert_pdf_to_json(passenger_entry_file, output_file, output_folder)


# move and delete files
def test_2():
    subfolder = 'passenger'
    pdf_dir = 'd:/Wiring Diagrams/Mercedes/{}/pdf'.format(subfolder)
    process_pdf_dir = 'd:/Wiring Diagrams/Mercedes/{}/processed_pdf'.format(subfolder)
    try:
        shutil.move(pdf_dir+'/file.txt', process_pdf_dir)
    except shutil.Error:
        os.remove(pdf_dir+'/file.txt')
        print('file exist')


# regex
def test_3():
    with open('d:/Wiring Diagrams/parsers/mercedes/titles.txt') as f:
        lines = f.readlines()
    titles = []
    for line in lines:
        match = re.search('[A-Z]\d\d[.]\d\d[-][A-Z]', line)
        if match:
            titles.append(line)
    print(titles)
    print(len(titles))


# edit cut cells in doc column
def test_4():
    subfolder = 'passenger'
    json_dir = 'd:/Wiring Diagrams/Mercedes/{}/json/file5.json'.format(subfolder)
    df = pd.read_json(json_dir)
    # print(df['1'].apply(type))
    for index, row in df.iterrows():
        if isinstance(row['1'], str):
            if len(row['1']) > len(row['doc']):
                # if re.search('[A-Z]\d\d[.]\d\d[-][A-Z]', row['1']):
                if row['doc'] in row['1']:
                    df.at[index, 'doc'] = row['1']
    # print(df.to_string())
    df.to_json(path_or_buf=json_dir)


# test clicker
def test_5():
    with open('d:/Wiring Diagrams/parsers/mercedes/titles3.txt') as f:
        lines = f.readlines()
    jsonfile = 'titles3.txt'
    for line in lines:
        clickbot(jsonfile, line)


# test click and keyboard
def test_6():
    kb.write('control module 1.10')
    kb.send('enter')


# check permission
def test_7():
    file = 'PE00.01-Z-0998AZ.pdf'
    path = f'D:/Wiring Diagrams/Mercedes/passenger/pdf/{file}'
    print('read: ' + str(os.access(path, os.R_OK)))
    print('write: ' + str(os.access(path, os.W_OK)))
    print('execute: ' + str(os.access(path, os.X_OK)))


# test clickbot for exist file by image and fit checkbox
def test_8():
    # docname = 'PE00.19-P-1100A'
    docname = 'PE72.29-P-2101-97DDA'
    jsonfile = 'not_found.json'
    clickbot(jsonfile, docname)


# test command line args
def test_9():
    start_from_json = False
    if len(sys.argv) > 1:
        if sys.argv[1] == 'json':
            start_from_json = True
    if start_from_json is True:
        print('json')
    else:
        print('pdf')


def main():
    # pandas settings
    pd.set_option('display.max_rows', None)
    pd.set_option('display.max_columns', None)
    pd.set_option('display.width', None)
    pd.set_option('display.max_colwidth', None)

    # click to wis
    # pi.click(468,1007)
    # test_9()
    work_loop()
    # pi.sleep(1)
    # click to VS Code
    # pi.click(395,1008)
    logging.info('program finish')

if __name__ == "__main__":
    main()